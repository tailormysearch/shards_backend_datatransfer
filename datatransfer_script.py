##
# Shards Backend Datatransfer
# 
# Transfers all the content from /data/ to our elasticsearch database 
##

from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import json, getpass, time, os

es_host = "search-es-product-tr65qzcnmz5c6tz4dkzkx2zuqm.eu-west-1.es.amazonaws.com"
es_port = 443
es_user = 'elastic' 
keys_file = "keys.txt"
willGet = True
data_folder = "data"

print("data transfer script started")
print("----------------------------")
print("host: %s" %(es_host))
print("port: %i\n" %(es_port))

def main():
    # IAM based authentication
    with open(keys_file) as f:
        keys = f.readlines()
    keys = [x.strip() for x in keys] 
    print("access Key: "+keys[0])
    awsauth = AWS4Auth(keys[0], keys[1], "eu-west-1", 'es')

    es = Elasticsearch( hosts = [{'host': es_host, 'port': es_port}],
                        http_auth=awsauth,
                        use_ssl=True,
                        verify_certs=True,
                        connection_class=RequestsHttpConnection)
    print(es.info())
    # Add data
    print("posting data...")
    for data_file in os.listdir(data_folder):
        print("folder: "+data_file)
        data = json.load(open(data_folder+"/"+data_file, 'r'))
        for item in data:
            res = es.index(index="resource", doc_type='product', id=item['id'], body=item)

    # get data
    if willGet:
        time.sleep(1)
        print("\ngetting data...")
        res = es.search(index="resource", body={"query": {"match_all": {}}})
        print("Got %d Hits:" % res['hits']['total'])
        for hit in res['hits']['hits']:
            print("\t-%(id)s %(name)s" % hit["_source"])
        print("\t...")
    print("\nscript finished succesfully.")

if __name__ == "__main__":
    main()