# Shards Backend DataTransfer #

Small datatransfer script to populate elasticsearch from the Product microservice, when it's active on AWS.

## Technologies ##

This script is written in pure [python](https://www.python.org/). To connect to the elasticsearch service, you will need to add your key as a textfile to the source file.


## Getting started ##

### Installation ###

Simply install python 2.3+ and you will be able to run the script. It only uses basic pyhton functionality.

### Run ###

Run the command `python datatransfer_script.py` in your console.

---